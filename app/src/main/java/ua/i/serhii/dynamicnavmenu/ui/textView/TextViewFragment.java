package ua.i.serhii.dynamicnavmenu.ui.textView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import ua.i.serhii.dynamicnavmenu.MainActivity;
import ua.i.serhii.dynamicnavmenu.R;
import ua.i.serhii.dynamicnavmenu.ui.Models.Item;

public class TextViewFragment extends Fragment {

    public static TextViewFragment getInstance() {return new TextViewFragment();}

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_gallery, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Item item = ((MainActivity)getActivity()).getItem();
        if(item != null)
            ((TextView)view.findViewById(R.id.text_gallery)).setText(item.get_param());

    }
}