package ua.i.serhii.dynamicnavmenu.ui.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Item implements Serializable {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("function")
    @Expose
    private String function;
    @SerializedName("param")
    @Expose
    private String param;

//    public Item(String _name, String _function, String _url) {
//        this.name = _name;
//        this.function = _function;
//        this.param = _url;
//    }

    public String get_name() {
        return name;
    }

    public void set_name(String _name) {
        this.name = _name;
    }

    public String get_function() {
        return function;
    }

    public void set_function(String _function) {
        this.function = _function;
    }

    public String get_param() {
        return param;
    }

    public void set_url(String _url) {
        this.param = _url;
    }
}
