package ua.i.serhii.dynamicnavmenu.ui.Listeners;


import ua.i.serhii.dynamicnavmenu.ui.Models.Item;

public interface OnClickMenuItem {
    void onClickMenuItem(final Item function, final int position);
}
