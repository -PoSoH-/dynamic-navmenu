package ua.i.serhii.dynamicnavmenu.ui.imageView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;

import ua.i.serhii.dynamicnavmenu.MainActivity;
import ua.i.serhii.dynamicnavmenu.R;
import ua.i.serhii.dynamicnavmenu.ui.Models.Item;

public class ImageViewFragment extends Fragment {

    public static ImageViewFragment getInstance(){return new ImageViewFragment();}

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_tools, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Item item = ((MainActivity)getActivity()).getItem();

        if(item != null)
            Picasso.get().
                    load(item.get_param()).
                    into((ImageView)view.findViewById(R.id.image_view));

    }
}