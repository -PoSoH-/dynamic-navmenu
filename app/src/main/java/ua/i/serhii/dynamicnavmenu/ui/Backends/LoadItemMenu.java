package ua.i.serhii.dynamicnavmenu.ui.Backends;

import retrofit2.Call;
import retrofit2.http.GET;
import ua.i.serhii.dynamicnavmenu.ui.Models.MenuDynamic;

public interface LoadItemMenu {

    //@FormUrlEncoded
    @GET("s/fk3d5kg6cptkpr6/menu.json?dl=1")  //(R.string.url_next)
    Call<MenuDynamic> getMenu();

}
