package ua.i.serhii.dynamicnavmenu.ui.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MenuDynamic implements Serializable {

    @SerializedName("menu")
    @Expose
    private List<Item> menu = null;

    public List<Item> getMenu() {
        return menu;
    }

    public void setMenu(List<Item> menu) {
        this.menu = menu;
    }
}
