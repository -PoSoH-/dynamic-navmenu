package ua.i.serhii.dynamicnavmenu;

import android.os.Bundle;

import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.i.serhii.dynamicnavmenu.ui.Adapters.MenuAdapters;
import ua.i.serhii.dynamicnavmenu.ui.Backends.LoadItemMenu;
import ua.i.serhii.dynamicnavmenu.ui.Listeners.OnClickMenuItem;
import ua.i.serhii.dynamicnavmenu.ui.Models.Item;
import ua.i.serhii.dynamicnavmenu.ui.Models.MenuDynamic;
import ua.i.serhii.dynamicnavmenu.ui.imageView.ImageViewFragment;
import ua.i.serhii.dynamicnavmenu.ui.textView.TextViewFragment;
import ua.i.serhii.dynamicnavmenu.ui.webView.WebViewFragment;

public class MainActivity extends AppCompatActivity {

    private MenuDynamic menuDynamic = null;
    private OnClickMenuItem listener;
    private MenuAdapters adapters;
    private RecyclerView menuView;
    private int currentMenuSelect = 0;
    private DrawerLayout drawer;
    private FragmentTransaction transaction;
    private Fragment frg = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);

        menuView = (RecyclerView) findViewById(R.id.menu_view) ;

        ActionBarDrawerToggle togle = new ActionBarDrawerToggle(this,
                drawer,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);

        togle.syncState();

        listener = new OnClickMenuItem() {
            @Override
            public void onClickMenuItem(Item function, int position) {
                currentMenuSelect = position;
                switchViewMenu();
            }
        };

        loadMenu();

    }

    private void switchViewMenu(){

        if(menuDynamic == null) return;
        switch(menuDynamic.getMenu().get(currentMenuSelect).get_function()){
            case "url":
//                Log.e("URL", currentMenuSelect);
                frg = WebViewFragment.getInstance();
                break;
            case "text":
//                Log.e("TEXT", currentMenuSelect);
                frg = TextViewFragment.getInstance();
                break;
            case "image":
//                Log.e("IMAGE", currentMenuSelect);
                frg = ImageViewFragment.getInstance();
                break;
        }
        drawer.closeDrawer(Gravity.LEFT);

        if(frg != null) {
            transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, frg, String.valueOf(currentMenuSelect));
            transaction.commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }



    public void onMenuListener(MenuItem item){
        switch (item.getItemId()){
            case R.id.action_settings:
                loadMenu();
                break;
        }
    }

    private void createDynamicMenu(){

        if(menuDynamic != null){
            RecyclerView.LayoutManager manager = new LinearLayoutManager(this);

            adapters = new MenuAdapters(menuDynamic, listener);
            menuView.setAdapter(adapters);
            menuView.setLayoutManager(manager);

        }

    }

    public Item getItem(){
        if(menuDynamic != null) {
            return menuDynamic.getMenu().get(currentMenuSelect);
        }
        return null;
    }

    private void loadMenu(){

        App.getBaseConnect().create(LoadItemMenu.class).getMenu().enqueue(new Callback<MenuDynamic>() {
            @Override
            public void onResponse(Call<MenuDynamic> call, Response<MenuDynamic> response) {
                if(response.isSuccessful()){

                    menuDynamic = response.body();

                    Log.e("SUCCES", response.body().toString());
                    createDynamicMenu();
                }else{
                    Log.e("ERROR", response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<MenuDynamic> call, Throwable t) {
                Log.e("FAILTURE", call.toString());
            }
        });

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if(menuDynamic == null) {
            menuDynamic = (MenuDynamic) savedInstanceState.getSerializable("MENU");
            currentMenuSelect = savedInstanceState.getInt("CURRENT", 0);

            createDynamicMenu();
            switchViewMenu();
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable("MENU", menuDynamic);
        outState.putInt("CURRENT", currentMenuSelect);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(frg != null) {
            transaction.remove(frg);
        }
    }
}
