package ua.i.serhii.dynamicnavmenu.ui.webView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import ua.i.serhii.dynamicnavmenu.MainActivity;
import ua.i.serhii.dynamicnavmenu.R;
import ua.i.serhii.dynamicnavmenu.ui.Models.Item;

public class WebViewFragment extends Fragment {

   public static WebViewFragment getInstance(){return new WebViewFragment();}

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_slideshow, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Item item =((MainActivity)getActivity()).getItem();
        WebView web = ((WebView)view.findViewById(R.id.web_view));
        if(item != null)
            web.loadUrl(item.get_param());
    }
}