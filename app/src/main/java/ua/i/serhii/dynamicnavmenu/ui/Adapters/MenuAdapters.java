package ua.i.serhii.dynamicnavmenu.ui.Adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ua.i.serhii.dynamicnavmenu.R;
import ua.i.serhii.dynamicnavmenu.ui.Listeners.OnClickMenuItem;
import ua.i.serhii.dynamicnavmenu.ui.Models.Item;
import ua.i.serhii.dynamicnavmenu.ui.Models.MenuDynamic;

public class MenuAdapters extends RecyclerView.Adapter<ItemHolder> {

    private MenuDynamic menuDynamic;
    private OnClickMenuItem listener;

    public MenuAdapters(MenuDynamic dynamic, OnClickMenuItem listener){
        menuDynamic = dynamic;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ItemHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemHolder holder, int position) {
        holder.setItem(menuDynamic.getMenu().get(position), listener, position);
//        listener.onClickMenuItem(holder.getItem());
    }

    @Override
    public int getItemCount() {
        return menuDynamic.getMenu().size();
    }
}

class ItemHolder extends RecyclerView.ViewHolder{

    private TextView textView;
    private Item item;
    private int position;
    private OnClickMenuItem listener;

    public ItemHolder(@NonNull final View itemView) {
        super(itemView);
        textView = itemView.findViewById(R.id.text_item);
    }

    public void setItem(Item item, OnClickMenuItem listener, final int position){
        this.item = item;
        textView.setText(item.get_name());
        this.listener = listener;
        createListener();
        this.position = position;
    }

    private void createListener(){
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Item click", item.get_name());
                listener.onClickMenuItem(item, position);
            }
        });
    }

    public Item getItem(){
        return item;
    }
}